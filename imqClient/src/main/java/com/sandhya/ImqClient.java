package com.sandhya;

import com.sandhya.exception.InvalidPortException;
import com.sandhya.exception.InvalidUserChoiceException;
import com.sandhya.handlers.PublisherHandler;
import com.sandhya.handlers.SubscriberHandler;
import org.jboss.logging.Logger;
import com.sandhya.userInterface.UserInterface;

import java.util.Scanner;

public class ImqClient {
    static Scanner input = new Scanner(System.in);

    public static void main(String args[]) throws InvalidPortException {
        final Logger logger = Logger.getLogger(ImqClient.class);
        String userInput = "";

        do {
            try {
                System.out.println("Enter the command to select pub or sub");
                userInput = input.next().toLowerCase();
                UserInterface userInterface = new UserInterface();
                String[] ary = userInterface.userCommand(userInput);
                if (ary[1].equals("pub")) {
                    new PublisherHandler();

                } else if (ary[1].equals("sub")) {
                    new SubscriberHandler();
                } else if (userInput.equals("c")) {
                    break;
                } else {
                    throw new InvalidUserChoiceException("Please chose a valid option");
                }

            } catch (Exception exception) {
                logger.debug("Connection problem");
                throw new InvalidPortException("Connection refused!");
            }
        } while (!(userInput == "p") || (userInput == "s"));
        logger.info("Have a wonderful day!!");
    }
}
