package com.sandhya.constants;

public class AppConstants {
    public static String HOST = "127.0.0.1";
    public static int PUBLISHER_PORT = 4444;
    public static int SUBSCRIBER_PORT = 1221;
}
