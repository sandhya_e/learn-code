package com.sandhya.handlers;

import com.sandhya.constants.AppConstants;
import com.sandhya.exception.InvalidTopicException;
import com.sandhya.exception.InvalidUserChoiceException;
import com.sandhya.services.PubSubService;
import com.sandhya.services.SubscriberService;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Scanner;

public class SubscriberHandler {

    static DataInputStream subInputStream = null;
    static DataOutputStream subscriberOutStrem = null;
    Scanner input = new Scanner(System.in);
    Socket socket;
    PubSubService clientService;
    SubscriberService subscriberService;

    public SubscriberHandler() throws IOException, SQLException {
        subscriberService = new SubscriberService(AppConstants.HOST, AppConstants.SUBSCRIBER_PORT);
        clientService = subscriberService;
        socket = subscriberService.CreateSocket();
        getSubscriberService(subscriberService, true);
    }

    public void getSubscriberService(SubscriberService subscriberService, boolean isValidClientAddress) throws IOException, SQLException {
        int userChoice;
        subscriberOutStrem = new DataOutputStream(socket.getOutputStream());
        subInputStream = new DataInputStream(socket.getInputStream());
        subscriberService.setClientData(getClientName(), subscriberOutStrem);
        int topicCount = subscriberService.getAllAvailableTopicNames(subInputStream);

        do {
            System.out.println("Press 1 to Enter topic and 2 to Close the connection");
            input = new Scanner(System.in);
            userChoice = input.nextInt();

            try {
                switch (userChoice) {
                    case 1: {
                        subscriberService.displayAvailableTopics();
                        int topicId = subscriberService.getTopicId();

                        if (topicId > topicCount) {
                            throw new InvalidTopicException("Please Enter a valid topic name");
                        } else {
                            subscriberService.sendSubscribedTopicId(subscriberOutStrem, topicId);
                            subscriberService.getData();
                            continue;
                        }
                    }

                    case 2: {
                        subscriberService.sendSubscribedTopicId(subscriberOutStrem, -1);
                        subscriberService.stop(subscriberOutStrem);
                        isValidClientAddress = false;
                        break;
                    }
                    default: {
                        isValidClientAddress = true;
                        throw new InvalidUserChoiceException("Please select a valid choice");
                    }
                }
            } catch (NumberFormatException exe) {
                System.out.println("Entered value is not a number");
            } catch (InvalidUserChoiceException invalidUserChoiceException) {
                invalidUserChoiceException.getMessage();
            } catch (InvalidTopicException invalidTopicException) {
                invalidTopicException.getMessage();
            } catch (Exception exception) {
                System.out.println(exception + "Something went wrong.. Please try again later");
            }
        } while (isValidClientAddress);
    }

    private String getClientName() {
        System.out.println("Enter your name :");
        String clientName = input.next();
        return clientName;
    }
}
