package com.sandhya.handlers;

import com.sandhya.dto.MessageDTO;
import com.sandhya.exception.InvalidTopicException;
import com.sandhya.exception.InvalidUserChoiceException;
import com.sandhya.constants.AppConstants;
import com.sandhya.services.PubSubService;
import com.sandhya.services.PublisherService;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Scanner;

public class PublisherHandler {
    static DataOutputStream pubOutStream = null;
    static DataInputStream pubInStream = null;
    Scanner input = new Scanner(System.in);
    Socket socket;
    PubSubService clientService;
    PublisherService publisherService;

    public PublisherHandler() throws IOException, SQLException {

        publisherService = new PublisherService(AppConstants.HOST, AppConstants.PUBLISHER_PORT);
        clientService = publisherService;
        socket = publisherService.CreateSocket();
        getService(publisherService, true);
    }

    public void getService(PublisherService publisherService, boolean isValidClientAddress) throws IOException, SQLException {

        int userChoice;
        pubOutStream = new DataOutputStream(socket.getOutputStream());
        pubInStream = new DataInputStream(socket.getInputStream());
        publisherService.setClientData(getClientName(), pubOutStream);
        int topicCount = publisherService.getAllAvailableTopicNames(pubInStream);

        do {
            System.out.println("Press 1 to enter message and 2 to close the connection");
            input = new Scanner(System.in);

            userChoice = input.nextInt();
            try {
                switch (userChoice) {
                    case 1: {
                        publisherService.displayAvailableTopics();
                        int topicId = publisherService.getTopicId();

                        if (topicId > topicCount) {
                            throw new InvalidTopicException("Please Enter a valid topic name");
                        } else {
                            String publisherData = publisherService.getData();
                            MessageDTO message = new MessageDTO();
                            message.setTopicId(topicId);
                            message.setMessage(publisherData);
                            publisherService.sendDataToServer(message, pubOutStream);
                            publisherService.getResponseFromServer(pubInStream);
                            continue;
                        }
                    }
                    case 2: {
                        MessageDTO message = new MessageDTO();
                        message.setMessage("quit");
                        publisherService.sendDataToServer(message, pubOutStream);
                        publisherService.stop(pubOutStream);
                        isValidClientAddress = false;
                        continue;
                    }
                    default: {
                        isValidClientAddress = true;
                        throw new InvalidUserChoiceException("Please select a valid choice");
                    }
                }
            } catch (InvalidUserChoiceException invalidUserChoiceException) {
                invalidUserChoiceException.getMessage();
            } catch (InvalidTopicException invalidTopicException) {
                invalidTopicException.getMessage();
            } catch (Exception exception) {
                System.out.println("Something went wrong. Please try again later!!!!");
            }
        } while (isValidClientAddress);
    }

    private String getClientName() {
        System.out.println("Enter your name :");
        String clientName = input.next();
        return clientName;
    }
}
