package com.sandhya.services;

import com.google.gson.Gson;
import com.sandhya.dto.MessageDTO;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;


public class PublisherService extends PubSubService {
    static Socket socket;
    private DataOutputStream output = null;

    public PublisherService(String ip, int port) throws IOException {
        super(ip, port);
    }

    public Socket CreateSocket() throws IOException {
        socket = createSocketConnection();
        return socket;
    }

    public String getData() throws IOException {
        String publisherData = null;
        System.out.println("Enter the data :");
        publisherData = super.scan.next();
        return publisherData;
    }

    public String serializedData(MessageDTO message) {
        Gson gson = new Gson();
        String serializedMessage = gson.toJson(message);
        return serializedMessage;
    }

    public void sendDataToServer(MessageDTO message, DataOutputStream output) throws IOException {
        output.writeUTF(serializedData(message));
    }

    public void getResponseFromServer(DataInputStream inputStream) throws IOException {
        String serverResponse;
        serverResponse = inputStream.readUTF();
        System.out.println("The data published is : " + serverResponse);
    }

    public void stop(DataOutputStream output) throws IOException {
        output.close();
        socket.close();
    }
}