package com.sandhya.services;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class SubscriberService extends PubSubService {
    static Socket socket;
    private DataInputStream subscriberInputStream = null;

    public SubscriberService(String ip, int port) throws IOException {
        super(ip, port);
    }

    public Socket CreateSocket() throws IOException {
        socket = createSocketConnection();
        return socket;
    }

    public void getData() throws IOException, ClassNotFoundException {
        subscriberInputStream = new DataInputStream(socket.getInputStream());
        String response = subscriberInputStream.readUTF();
        System.out.println("Your subscribed message is:");
        System.out.println(response);
    }

    public void sendSubscribedTopicId(DataOutputStream outputStream, int topicId) throws IOException {
        outputStream.writeInt(topicId);
    }

    public void stop(DataOutputStream outputStream) throws IOException {
        outputStream.close();
        socket.close();
    }
}
