package com.sandhya.userInterface;

import java.util.Arrays;
import java.util.List;

public class UserInterface {

    public String[] userCommand(String userInput) {

        List<String> cmdlist = Arrays.asList(new String[]{"pub", "sub"});

        if (userInput.startsWith("imq")) {

            String[] array = new String[2];
            array = userInput.split("-");
            if (cmdlist.contains(array[1])) {
                return array;
            } else {
                array[0] = "c";
                return array;
            }
        } else {
            System.out.println("Wrong cmd enter valid command!!");
        }
        return null;
    }
}