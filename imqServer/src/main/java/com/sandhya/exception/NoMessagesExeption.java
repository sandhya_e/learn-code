package com.sandhya.exception;

public class NoMessagesExeption extends Exception {
    public NoMessagesExeption(String errorMessage) {
        super(errorMessage);
    }
}
