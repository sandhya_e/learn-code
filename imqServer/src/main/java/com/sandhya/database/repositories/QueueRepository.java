package com.sandhya.database.repositories;

import com.sandhya.database.connectionmanager.ConnectionManagerFactory;
import com.sandhya.database.connectionmanager.DatabaseConnectionManager;
import com.sandhya.database.enums.DatabaseType;
import com.sandhya.dto.MessageDTO;
import com.sandhya.protocol.IMQProtocol;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class QueueRepository {
    private DatabaseConnectionManager connectionManager;
    private Connection connection;

    public QueueRepository() {
        connectionManager = ConnectionManagerFactory.getInstance(DatabaseType.MY_SQL);
        try {
            connection = connectionManager.getConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean insert(IMQProtocol message, int topicId) {
        MessageDTO imqMessage = new MessageDTO();
        imqMessage.setTopicId(topicId);
        imqMessage.setMessage(message.getData());
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO MessageQueue(topicId,message,timeStamp) VALUES (?,?,?);");
            preparedStatement.setInt(1, imqMessage.getTopicId());
            preparedStatement.setString(2, imqMessage.getMessage());
            preparedStatement.setString(3, message.getCreateTime());
            preparedStatement.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ResultSet findMessagesByTopicId(int topicId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select message from database.messagequeue where topicId = ? limit 1;");
            preparedStatement.setInt(1, topicId);
            ResultSet result = preparedStatement.executeQuery();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean deleteTopMessage(int topicId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM MessageQueue WHERE topicId=? LIMIT 1 ;");
            preparedStatement.setInt(1, topicId);
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean deleteTimedOutMessage(int id) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("Insert into DeadLetterQueue(id,topicId,message,timeStamp) (select id,topicId,message,timeStamp from MessageQueue where id = ?);");
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            PreparedStatement preparedStatement1 = connection.prepareStatement("Delete from MessageQueue where id = ?;");
            preparedStatement1.setInt(1, id);
            preparedStatement1.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public ResultSet getAllMessages() {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("Select id,timeStamp from MessageQueue;");
            ResultSet result = preparedStatement.executeQuery();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
