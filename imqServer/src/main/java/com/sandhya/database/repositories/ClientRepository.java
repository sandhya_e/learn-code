package com.sandhya.database.repositories;

import com.sandhya.database.connectionmanager.ConnectionManagerFactory;
import com.sandhya.database.connectionmanager.DatabaseConnectionManager;
import com.sandhya.database.enums.DatabaseType;
import com.sandhya.dto.ClientDataDTO;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

public class ClientRepository {

    private DatabaseConnectionManager connectionManager;
    private Connection connection;

    public ClientRepository() {

        connectionManager = ConnectionManagerFactory.getInstance(DatabaseType.MY_SQL);
        try {
            connection = connectionManager.getConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean insert(String clientName, String clientType) {
        ClientDataDTO clientData = new ClientDataDTO();
        clientData.setClientName(clientName);
        clientData.setClientType(clientType);
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO ClientData(clientName,clientType) VALUES (?,?);");
            preparedStatement.setString(1, clientData.getClientName());
            preparedStatement.setString(2, clientData.getClientType());
            preparedStatement.executeUpdate();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
}
