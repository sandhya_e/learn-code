package com.sandhya.database.repositories;

import com.sandhya.database.connectionmanager.ConnectionManagerFactory;
import com.sandhya.database.connectionmanager.DatabaseConnectionManager;
import com.sandhya.database.enums.DatabaseType;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class TopicRepository {

    private DatabaseConnectionManager connectionManager;
    private Connection connection;

    public TopicRepository() {

        connectionManager = ConnectionManagerFactory.getInstance(DatabaseType.MY_SQL);
        try {
            connection = connectionManager.getConnection();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public ArrayList getAvailableTopics() throws SQLException {
        ArrayList availableTopics = new ArrayList();
        java.sql.Statement statement = connection.createStatement();
        ResultSet resultList = statement.executeQuery("SELECT topic_name FROM Topic");
        while (resultList.next()) {
            availableTopics.add(resultList.getString(1));
        }
        return availableTopics;
    }

    public int findTopicId(String topicName) throws SQLException {
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT id FROM Topic WHERE topic_name = ?");
        preparedStatement.setString(1, topicName);
        ResultSet resultList = preparedStatement.executeQuery();
        resultList.next();
        return resultList.getInt("id");
    }
}
