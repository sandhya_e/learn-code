package com.sandhya.database.connectionmanager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public interface DatabaseConnectionManager {
    Connection getConnection() throws IOException;

    void closeConnection() throws SQLException;
}
