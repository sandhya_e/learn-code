package com.sandhya.database.connectionmanager;


import com.sandhya.constants.AppConstants;
import com.sandhya.database.connectionmanager.DatabaseConnectionManager;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class MySqlConnectionManager implements DatabaseConnectionManager {
    private Connection connection;

    @Override
    public Connection getConnection() throws IOException {
        try {
            connection = DriverManager.getConnection(AppConstants.CONNECTION_URL, AppConstants.USERNAME, AppConstants.PWD);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    @Override
    public void closeConnection() throws SQLException {
        connection.close();
    }
}
