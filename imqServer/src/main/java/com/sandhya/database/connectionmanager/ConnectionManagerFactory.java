package com.sandhya.database.connectionmanager;

import com.sandhya.database.enums.DatabaseType;

public class ConnectionManagerFactory {
    public static DatabaseConnectionManager getInstance(DatabaseType type) {
        switch (type) {
            case MY_SQL:
                return new MySqlConnectionManager();
            default:
                return new MySqlConnectionManager();
        }
    }
}
