package com.sandhya;

import com.sandhya.exception.InvalidPortException;
import com.sandhya.handlers.PublisherHandler;
import com.sandhya.handlers.SubscriberHandler;
import com.sandhya.services.QueueService;
import org.jboss.logging.Logger;

import java.sql.SQLException;
import java.text.ParseException;

public class ImqServer {
    public static void main(String args[]) throws InvalidPortException {
        System.out.println("Server Started..");

        final Logger logger = Logger.getLogger(ImqServer.class);
        logger.info("Sever started successfully!!!!!");

        Thread publisherThread = new Thread() {
            public void run() {
                try {
                    new PublisherHandler();
                } catch (InvalidPortException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread subscriberThread = new Thread() {
            public void run() {
                try {
                    new SubscriberHandler();
                } catch (InvalidPortException e) {
                    e.printStackTrace();
                }
            }
        };
        Thread deadLetterQueueThread = new Thread() {
            public void run() {
                try {
                    while (true) {
                        deadLetterQueueHandler();
                        Thread.sleep(20000);
                    }
                } catch (Exception exe) {
                    exe.printStackTrace();
                }
            }
        };

        publisherThread.start();
        subscriberThread.start();
        deadLetterQueueThread.start();
    }

    private static void deadLetterQueueHandler() throws SQLException, ParseException, InterruptedException, org.json.simple.parser.ParseException {
        System.out.println("Traversing queue for dead messages");
        QueueService queueService = new QueueService();
        queueService.deadLetterQueueHandler();
    }
}
