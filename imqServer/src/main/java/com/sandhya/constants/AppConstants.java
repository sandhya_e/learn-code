package com.sandhya.constants;

public class AppConstants {
    public static String host = "127.0.0.1";
    public static int PUBLISHER_PORT = 4444;
    public static int SUBSCRIBER_PORT = 1221;
    public static String USERNAME = "root";
    public static String PWD = "root";
    public static final String CONNECTION_URL = "jdbc:mysql://localhost:3306/database?useSSL=false";
}
