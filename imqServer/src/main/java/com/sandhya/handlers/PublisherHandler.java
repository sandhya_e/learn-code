package com.sandhya.handlers;

import com.sandhya.constants.AppConstants;
import com.sandhya.exception.InvalidPortException;
import com.sandhya.services.PublisherService;

import java.net.ServerSocket;
import java.net.Socket;

public class PublisherHandler {
    public PublisherHandler() throws InvalidPortException {
        handlePublishers();
    }

    private void handlePublishers() throws InvalidPortException {
        try {
            ServerSocket serverPublisherSocket = new ServerSocket(AppConstants.PUBLISHER_PORT);
            while (true) {
                Socket publisherSocket = serverPublisherSocket.accept();
                if (publisherSocket.getLocalPort() == AppConstants.PUBLISHER_PORT) {
                    PublisherService publisherHandler = new PublisherService(publisherSocket);
                    publisherHandler.start();
                }
            }
        } catch (Exception exception) {
            throw new InvalidPortException("Connection is refused!!");
        }
    }
}
