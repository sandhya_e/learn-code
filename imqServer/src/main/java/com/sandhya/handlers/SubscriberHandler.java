package com.sandhya.handlers;

import com.sandhya.constants.AppConstants;
import com.sandhya.exception.InvalidPortException;
import com.sandhya.services.SubscriberService;

import java.net.ServerSocket;
import java.net.Socket;

public class SubscriberHandler {
    public SubscriberHandler() throws InvalidPortException {
        handleSubscribers();
    }

    private void handleSubscribers() throws InvalidPortException {
        try {
            ServerSocket serverSubscriberSocket = new ServerSocket(AppConstants.SUBSCRIBER_PORT);
            while (true) {
                Socket subscriberSocket = serverSubscriberSocket.accept();
                if (subscriberSocket.getLocalPort() == AppConstants.SUBSCRIBER_PORT) {
                    SubscriberService subscriberHandler = new SubscriberService(subscriberSocket);
                    subscriberHandler.start();
                }
            }
        } catch (Exception exception) {
            throw new InvalidPortException("Connection is refused!!");
        }
    }
}
