package com.sandhya.services;

import com.google.gson.JsonSyntaxException;
import com.sandhya.dto.MessageDTO;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.sandhya.protocol.IMQProtocol;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Date;


public class PublisherService extends Thread {
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream output = null;
    QueueService queueService = new QueueService();

    public PublisherService(Socket publisherSocket) {
        this.socket = publisherSocket;
        try {
            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public MessageDTO getPublisherData() throws IOException, ClassNotFoundException {
        String publisherData = null;
        MessageDTO message = new MessageDTO();
        publisherData = input.readUTF();
        try {
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(publisherData);
            message.setMessage((json.get("message")).toString());
            message.setTopicId(Integer.parseInt(json.get("topicId").toString()));

            return message;
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return message;
    }

    public void sendPublisherResponse(String message, DataOutputStream output) throws IOException, JsonSyntaxException, SQLException {
        output.writeUTF((String) message);
    }

    public void savePublishedData(IMQProtocol message, int topicId) {
        queueService.pushMessageToQueue(message, topicId);
    }

    public void run() {
        IMQProtocol messageObject = null;
        MessageDTO message = new MessageDTO();

        try {
            saveClientName(getClientName());
            sendAvailableTopics();

        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }

        do {
            try {
                message = getPublisherData();

                if (message.getMessage().equals("quit")) {
                    continue;
                } else {
                    messageObject = new IMQProtocol();
                    messageObject.setData(message.getMessage());
                    messageObject.setCreateTime(queueService.formatter.format(new Date()));
                    savePublishedData(messageObject, message.getTopicId());
                    sendPublisherResponse(message.getMessage(), output);
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!(message.getMessage().equals("quit")));
    }

    private void sendAvailableTopics() throws IOException, SQLException {
        output.writeUTF(queueService.getAvailableTopics());
    }

    private void saveClientName(String clientName) {
        queueService.insertClientName(clientName, "Publisher");
    }

    private String getClientName() throws IOException {
        String clientName = input.readUTF();
        return clientName;
    }
}