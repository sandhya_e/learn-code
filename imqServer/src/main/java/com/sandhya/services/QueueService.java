package com.sandhya.services;

import com.google.gson.Gson;
import com.sandhya.database.repositories.ClientRepository;
import com.sandhya.database.repositories.QueueRepository;
import com.sandhya.database.repositories.TopicRepository;
import com.sandhya.exception.NoMessagesExeption;
import org.json.simple.parser.ParseException;
import com.sandhya.protocol.IMQProtocol;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class QueueService {
    QueueRepository queueDataObject = new QueueRepository();
    ClientRepository clientRepository = new ClientRepository();
    TopicRepository topicRepository = new TopicRepository();

    SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");

    public boolean pushMessageToQueue(IMQProtocol message, int topicId) {
        return queueDataObject.insert(message, topicId);
    }

    public ArrayList pullAllMessageForTopicFromQueue(int topicId) throws InterruptedException, SQLException {
        ArrayList m = new ArrayList();
        ResultSet rs = queueDataObject.findMessagesByTopicId(topicId);
        while (rs.next()) {
            m.add(rs.getString(1));
        }
        return m;
    }

    public String pullMessageFromQueue(int topicId) throws InterruptedException, SQLException, NoMessagesExeption {
        String pulledMessage;
        ResultSet rs = queueDataObject.findMessagesByTopicId(topicId);
        if (rs.next()) {
            pulledMessage = rs.getString(1);
            return pulledMessage;
        } else {
            throw new NoMessagesExeption("No recent messages");
        }
    }

    private void deletePulledMessage(int topicId) {
        queueDataObject.deleteTopMessage(topicId);
    }

    public void insertClientName(String clientName, String clientType) {
        clientRepository.insert(clientName, clientType);
    }

    public String getAvailableTopics() throws SQLException {
        ArrayList topics = topicRepository.getAvailableTopics();
        String availableTopics = new Gson().toJson(topics);
        return availableTopics;
    }

    public void deadLetterQueueHandler() throws SQLException, ParseException, java.text.ParseException {
        ResultSet allMessages = queueDataObject.getAllMessages();
        while (allMessages.next()) {
            String createdTime = allMessages.getString(2);
            int id = allMessages.getInt(1);
            Date now = formatter.parse(formatter.format(new Date()));
            Date createdTimeFromDB = formatter.parse(createdTime);
            long duration = now.getTime() - createdTimeFromDB.getTime();
            long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
            if (diffInMinutes >= 2) {
                queueDataObject.deleteTimedOutMessage(id);
                System.out.println("Moved the message of id " + id + " to dead letter queue");
            }
        }
    }
}
