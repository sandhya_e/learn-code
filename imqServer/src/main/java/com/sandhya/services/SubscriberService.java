package com.sandhya.services;

import com.sandhya.exception.NoMessagesExeption;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;

public class SubscriberService extends Thread {
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream output = null;
    QueueService queueService = new QueueService();

    public SubscriberService(Socket socket) {
        this.socket = socket;
        try {
            input = new DataInputStream(socket.getInputStream());
            output = new DataOutputStream(socket.getOutputStream());
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    public int getSubscriberTopicId() throws IOException, ClassNotFoundException {
        int subscriberId;
        subscriberId = input.readInt();
        return subscriberId;
    }

    public String sendSubscribedData(int topicId) throws InterruptedException, SQLException, NoMessagesExeption {
        String message = queueService.pullMessageFromQueue(topicId);
        return message;
    }

    public void run() {
        int topicId = 0;
        try {
            saveClientName(getClientName());
            sendAvailableTopics();
        } catch (IOException e1) {
            e1.printStackTrace();
        } catch (SQLException e1) {
            e1.printStackTrace();
        }
        do {
            try {
                topicId = getSubscriberTopicId();
                if (topicId == -1) {
                    continue;
                } else {
                    output.writeUTF(sendSubscribedData(topicId));
                    queueService.deadLetterQueueHandler();
                }
            } catch (IOException exception) {
                exception.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } while (!(topicId == -1));
    }

    private void sendAvailableTopics() throws IOException, SQLException {
        output.writeUTF(queueService.getAvailableTopics());
    }

    private void saveClientName(String clientName) {
        queueService.insertClientName(clientName, "Subscriber");
    }

    private String getClientName() throws IOException {
        String clientName = input.readUTF();
        return clientName;
    }
}