package com.sandhya.dto;

import java.io.Serializable;

public class MessageDTO implements Serializable {
    private int topicId;
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }
}
