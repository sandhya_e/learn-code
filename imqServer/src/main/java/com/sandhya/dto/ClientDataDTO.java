package com.sandhya.dto;

public class ClientDataDTO {
    private int clientId;
    private String clientName;
    private String clientType;
    private int topicId;

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public String setClientName(String clientName) {
        this.clientName = clientName;
        return clientName;
    }

    public String getClientType() {
        return clientType;
    }

    public String setClientType(String clientType) {
        this.clientType = clientType;
        return clientType;
    }

    public int getTopicId() {
        return topicId;
    }

    public void setTopicId(int topicId) {
        this.topicId = topicId;
    }
}