package com.sandhya.database;

import com.sandhya.database.repositories.QueueRepository;
import com.sandhya.database.repositories.TopicRepository;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;
import com.sandhya.protocol.IMQProtocol;

import java.sql.SQLException;

public class QueueRepositoryTest
{
	QueueRepository queueDataObject;
	TopicRepository topicRepository;

	@Before
	public void setUp()
	{
		queueDataObject = new QueueRepository();
		topicRepository = new TopicRepository();
	}

	@Test
	public void testInsertMessage() {
		IMQProtocol message = new IMQProtocol();
		message.setCreateTime("27/02/2021 08:19:50");
		message.setData("Testing");
		boolean actual = queueDataObject.insert(message, 100);
		boolean expected = true;
		Assert.assertEquals(expected, actual);
	}

	@Test
	public void testFindTopicId() throws SQLException {
		int actual = topicRepository.findTopicId("Java");
		Assert.assertEquals(1, actual);
	}

	@Test
	public void testDeleteTopMessage()
	{
		boolean actual = queueDataObject.deleteTopMessage(1);
		boolean expected = true;
		Assert.assertEquals(expected, actual);
	}
}