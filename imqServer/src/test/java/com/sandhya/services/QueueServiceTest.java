package com.sandhya.services;

import com.sandhya.database.connectionmanager.ConnectionManagerFactory;
import com.sandhya.database.connectionmanager.DatabaseConnectionManager;
import com.sandhya.database.connectionmanager.MySqlConnectionManager;
import com.sandhya.database.enums.DatabaseType;
import com.sandhya.protocol.IMQProtocol;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.SQLException;

public class QueueServiceTest {

    QueueService queueService;
    private DatabaseConnectionManager connectionManager;

    @Before
    public void createObject() {
        queueService = new QueueService();
        connectionManager = new MySqlConnectionManager() {
        };
    }

    @Test
    public void testGetDatabaseConnection() {
        DatabaseConnectionManager connectionManager = ConnectionManagerFactory.getInstance(DatabaseType.MY_SQL);
        Assert.assertNotNull(connectionManager);
    }

    @Test
    public void testPushMessageToQueue() {
        IMQProtocol message = new IMQProtocol();
        message.setData("test data");
        message.setCreateTime("27/02/2021 08:19:50");
        boolean actual = queueService.pushMessageToQueue(message, 100);
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testGetAvailableTopics() throws SQLException {
        String actual = queueService.getAvailableTopics();
        String expected = "[\"Java\",\"Python\"]";
        Assert.assertEquals(expected, actual);
    }
}
