package com.sandhya.repositories;

import com.sandhya.database.repositories.ClientRepository;
import com.sandhya.dto.ClientDataDTO;
import junit.framework.Assert;
import org.junit.Before;
import org.junit.Test;

public class ClientRepositoryTest {

    ClientRepository clientRepository;
    ClientDataDTO clientDataDTO;

    @Before
    public void setUp() {
        clientRepository = new ClientRepository();
        clientDataDTO = new ClientDataDTO();
    }

    @Test
    public void testInsertClient() {
        boolean actual = clientRepository.insert(clientDataDTO.setClientName("Pub"), clientDataDTO.setClientType("Publisher"));
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }

    @Test
    public void testInsert() {
        boolean actual = clientRepository.insert(clientDataDTO.setClientName("Sub"), clientDataDTO.setClientType("Subscriber"));
        boolean expected = true;
        Assert.assertEquals(expected, actual);
    }
}
